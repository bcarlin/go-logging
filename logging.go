package logging

import (
	"errors"
	"fmt"
	"strings"
	"sync"
)

var (
	loggers map[string]*Logger
	lock    sync.Mutex
)

type Level byte

const (
	DEBUG Level = iota
	INFO
	WARNING
	ERROR
	CRITICAL
	FATAL
	DEFAULT_LEVEL = INFO
)

var levelNames = [6]string{"DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL", "FATAL"}

func (l Level) Name() string {
	return levelNames[l]
}

func LevelByName(l string) (Level, error) {
	for pos, name := range levelNames {
		if name == l {
			return Level(pos), nil
		}
	}
	return DEBUG, errors.New(fmt.Sprintf("Invalid log level %s", l))
}

type Formatter func(*Record) string

func GetLogger(name string) (l *Logger) {
	lock.Lock()
	defer lock.Unlock()

	if name == "" {
		name = "default"
	}
	l, ok := loggers[name]
	if !ok {
		l = NewLogger(name)
		backend := NewStdoutBackend()
		l.AddBackend(backend)
		l.SetLevel(DEBUG)
		loggers[name] = l
	}
	return l
}

var defaultFormatter Formatter = func(r *Record) string {
	return fmt.Sprintf("%s [%-8s] %s: %s\n",
		r.Timestamp.Format("2006/01/02 15:04:05"), r.Level.Name(), r.Logger,
		strings.TrimSpace(r.Message))
}

var basicFormatter Formatter = func(r *Record) string {
	return fmt.Sprintf("%s: %s", r.Logger, strings.TrimSpace(r.Message))
}

func init() {
	loggers = make(map[string]*Logger, 3)

	logger := NewLogger("default")
	backend := NewStdoutBackend()
	logger.AddBackend(backend)
	logger.SetLevel(DEBUG)

	loggers["default"] = logger
}
