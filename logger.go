package logging

import (
	"fmt"
	"log"
	"os"
	"sync"
)

type Logger struct {
	// added to avoid concurrent writes
	sync.Mutex

	name     string
	level    Level
	backends []Backend
}

func NewLogger(name string) (l *Logger) {
	l = &Logger{
		name:  name,
		level: DEFAULT_LEVEL,
	}
	return
}

// Add a new Backend to the logger
func (l *Logger) AddBackend(b Backend) {
	l.backends = append(l.backends, b)
}

// Sets the backend list to the logger. Any existing backend will be lost
func (l *Logger) SetBackend(b ...Backend) {
	l.backends = b
}

func (l *Logger) SetLevel(level Level) {
	l.level = level
	for _, backend := range l.backends {
		backend.SetLevel(level)
	}
}

type buffer struct {
	level  Level
	logger *Logger
}

func (b *buffer) Write(p []byte) (n int, err error) {
	b.logger.Log(b.level, string(p))
	return len(p), nil
}

func (l *Logger) AsStdLog(level Level) *log.Logger {
	stdLogger := log.New(&buffer{logger: l, level: level}, "", 0)

	return stdLogger
}

func (l *Logger) Log(level Level, m string) {
	l.Lock()
	defer l.Unlock()

	r := NewRecord(l.name, level, m)
	for _, backend := range l.backends {
		if r.Level >= backend.Level() {
			backend.Write(r)
		}
	}
}

func (l *Logger) Debug(text string) {
	l.Log(DEBUG, text)
}

func (l *Logger) Debugf(text string, args ...interface{}) {
	l.Debug(fmt.Sprintf(text, args...))
}

func (l *Logger) Info(text string) {
	l.Log(INFO, text)
}

func (l *Logger) Infof(text string, args ...interface{}) {
	l.Info(fmt.Sprintf(text, args...))
}

func (l *Logger) Warning(text string) {
	l.Log(WARNING, text)
}

func (l *Logger) Warningf(text string, args ...interface{}) {
	l.Warning(fmt.Sprintf(text, args...))
}

func (l *Logger) Error(text string) {
	l.Log(ERROR, text)
}

func (l *Logger) Errorf(text string, args ...interface{}) {
	l.Error(fmt.Sprintf(text, args...))
}

func (l *Logger) Critical(text string) {
	l.Log(CRITICAL, text)
}

func (l *Logger) Criticalf(text string, args ...interface{}) {
	l.Critical(fmt.Sprintf(text, args...))
}

func (l *Logger) Fatal(text string) {
	l.Log(DEBUG, text)
	os.Exit(100)
}

func (l *Logger) Fatalf(text string, args ...interface{}) {
	l.Fatal(fmt.Sprintf(text, args...))
}
