package logging

import "testing"

func Test_LevelByName(t *testing.T) {
	for _, levelName := range levelNames {
		l, e := LevelByName(levelName)
		if e != nil {
			t.Errorf("level %s not recognized", levelName)
		}
		if l.Name() != levelName {
			t.Errorf("expected '%s', got '%s'", levelName, l.Name())
		}
	}
}
