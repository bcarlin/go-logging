package logging

import (
	"time"
)

type Record struct {
	Logger    string
	Timestamp time.Time
	Level     Level
	Message   string
}

func NewRecord(name string, l Level, m string) (r *Record) {
	r = &Record{
		Logger:    name,
		Level:     l,
		Message:   m,
		Timestamp: time.Now(),
	}
	return
}
